module.exports = (grunt) ->
    grunt.initConfig
        pkg: grunt.file.readJSON 'package.json'
        coffee:
            coffee_to_js:
                options:
                    bare: true
                expand: true
                flatten: false
                cwd: 'src'
                src: ["**/*.coffee"]
                dest: 'target'
                ext: '.js'
        copy:
            main:
                files: [
                    expand: true
                    cwd: 'src'
                    src: ['**/*.py']
                    dest: 'target'
                    filter: 'isFile'
                ]

    grunt.loadNpmTasks 'grunt-contrib-coffee'
    grunt.loadNpmTasks 'grunt-contrib-copy'
    grunt.registerTask 'compile', ['coffee', 'copy']