module.exports = {
  development: {
    protocol: 'http',
    host: '127.0.0.1',
    port: 1337,
    verifyToken: 'bfab20e0-bb96-11e5-8ee7-6fb3a93b838e',
    devices: [
      {
        devId: 'c35fbe10-bb8e-11e5-be2c-a70bb4873776',
        type: 'relay',
        pin: 11
      }, {
        devId: 'cb6ee220-bb8e-11e5-be2c-a70bb4873776',
        type: 'relay',
        pin: 12
      }, {
        devId: 'cbd1e9b0-bb8e-11e5-be2c-a70bb4873776',
        type: 'relay',
        pin: 13
      }, {
        devId: 'cc33dfd0-bb8e-11e5-be2c-a70bb4873776',
        type: 'relay',
        pin: 14
      }
    ]
  },
  production: {
    protocol: 'http',
    host: 'smarthome.vnsaas.com',
    port: 1337,
    verifyToken: 'd3fdbfa0-bc61-11e5-950b-3d11bcffca34',
    devices: [
      {
        devId: 'c35fbe10-bb8e-11e5-be2c-a70bb4873776',
        type: 'relay',
        pin: 17
      }, {
        devId: 'cb6ee220-bb8e-11e5-be2c-a70bb4873776',
        type: 'relay',
        pin: 18
      }, {
        devId: 'cbd1e9b0-bb8e-11e5-be2c-a70bb4873776',
        type: 'relay',
        pin: 22
      }, {
        devId: 'cc33dfd0-bb8e-11e5-be2c-a70bb4873776',
        type: 'relay',
        pin: 23
      }
    ]
  }
};
