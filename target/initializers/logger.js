var winston;

winston = require('winston');

module.exports.start = function() {
  return global.logger = new winston.Logger({
    transports: [
      new winston.transports.Console(), new winston.transports.File({
        filename: 'node.log'
      })
    ]
  });
};
