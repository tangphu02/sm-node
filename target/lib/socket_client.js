var io;

io = require('socket.io-client');

module.exports.start = function() {
  var socket;
  socket = io.connect(config.protocol + "://" + config.host + ":" + config.port, {
    reconnect: true,
    query: "verifyToken=" + config.verifyToken
  });
  socket.on('connect', function() {
    logger.info('Connected!');
    return socket.emit('end-point-info', config.devices);
  });
  socket.on('error', function(err) {
    return logger.error(err);
  });
  socket.on('disconnect', function() {
    return logger.info('Disconnect');
  });
  return require("./devices")(socket);
};
