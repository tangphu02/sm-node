var _, exec, sys;

_ = require('underscore');

sys = require('sys');

exec = require('child_process').exec;

module.exports = function(socket) {
  return socket.on('end-point-click', function(data) {
    var device;
    device = _.find(config.devices, function(device) {
      return device.devId === data.devId;
    });
    if (device && device.type === 'relay') {
      return exec("sudo python relay.py " + device.pin + " " + data.status);
    }
  });
};
