# Copyright (c) SmartHome 2016
# All rights are reserved.

winston = require 'winston'

module.exports.start = () ->
  global.logger = new (winston.Logger)({
    transports: [
      new (winston.transports.Console)(),
      new (winston.transports.File)({ filename: 'node.log' })
    ]
  })
