# Copyright (c) SmartHome 2016
# All rights are reserved.

io = require('socket.io-client')

module.exports.start = ->
    socket = io.connect("#{config.protocol}://#{config.host}:#{config.port}", {
      reconnect: true,
      query: "verifyToken=#{config.verifyToken}"
    })

    socket.on 'connect', () ->
        logger.info 'Connected!'
        socket.emit 'end-point-info', config.devices

    socket.on 'error', (err) ->
        logger.error err

    socket.on 'disconnect', () ->
        logger.info 'Disconnect'

    require("./devices")(socket)
