# Copyright (c) SmartHome 2016
# All rights are reserved.

_ = require 'underscore'
sys = require 'sys'
exec = require('child_process').exec

module.exports = (socket) ->
    socket.on 'end-point-click', (data) ->
        device = _.find config.devices, (device) -> device.devId == data.devId
        if device && device.type == 'relay'
            # logger.info "sudo python relay.py #{device.pin} #{data.status}"
            exec "sudo python relay.py #{device.pin} #{data.status}"
