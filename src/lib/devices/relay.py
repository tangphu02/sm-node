#!/usr/bin/python
import sys
import RPi.GPIO as GPIO

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)

args = sys.argv
pin = args[1] #Argument 1 for pin
ctl = args[2] #Argument 2 for ON/OFF
if (ctl == 'on'):
  GPIO.setup(pin, GPIO.OUT)
  GPIO.output(pin, GPIO.HIGH)

if (ctl == 'off'):
  GPIO.setup(pin, GPIO.OUT)
  GPIO.output(pin, GPIO.LOW)
