# Copyright (c) SmartHome 2016
# All rights are reserved.

path = require "path"
env = process.env.NODE_ENV || 'development'
global.config = require('./config/settings')[env]
global.appRoot = path.resolve(__dirname)

require('./initializers/logger').start()
require('./lib/socket_client').start()

logger.info "Stage: #{env}"
logger.info "Global Config:", global.config
